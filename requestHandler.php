<?php

use function PHPSTORM_META\elementType;

$folderToCreate = [];

if (isset($_POST['name'])) {

	$dir = $_POST['name'];
	parseParrentFolder($dir , '', $folderToCreate);


	foreach($folderToCreate as $folderPath){
		$folderPath = 	str_replace(' ', '', $folderPath);
		mkdir($folderPath, 0700, true);
		echo ('created  -' . $folderPath . '<br>' );
	}


}


function parseParrentFolder($string, $header = '' , &$arr)
{

	$boolparse = false;
	$length = strlen($string);
	$leftCounter = 0;
	$rightCounter = 0;
	$newPosition = 0;
	if ($length !== 0) {

		for ($i = 0; $i < $length; $i++) {

			# code...
			if ($string[$i] == '(') {
				$leftCounter++;
				$newPosition = $i;
			

			}
			if ($string[$i] == ')') {
				$rightCounter++;
				$boolparse = true;
			}

			if ($rightCounter !== 0 && $boolparse && $leftCounter == $rightCounter) {

				$element = substr($string, $newPosition + 1, $i - $newPosition - 1);  // get string without main ( )
				$folderNameResult = [];

				preg_match('/(.*?)\[/', $element, $folderNameResult);
				$header .=  $folderNameResult[1] . '/';

				$element = substr($element, strlen($folderNameResult[1])); // left [ ] without parrent folder name


				parseChildFolder($element, $header ,  $arr);


				$header = ''; // restore headername to anoher iteration.


				$boolparse = false;
			}
		}
	}
}



function parseChildFolder($string, $header = '',  &$arr)
{



	$boolparse = false;
	$length = strlen($string);
	$leftCounter = 0;
	$rightCounter = 0;
	$newPosition = 0;
	if ($length !== 0) {

		for ($i = 0; $i < $length; $i++) {

			
			if ($string[$i] == '[') {
				$leftCounter++;
				$newPosition = $i;
			

			}
			if ($string[$i] == ']') {
				$rightCounter++;
				$boolparse = true;
			}

			if ($rightCounter !== 0 && $boolparse && $leftCounter == $rightCounter) {

				$insideFolders = substr($string, $newPosition + 1, $i - $newPosition - 1) ;

				if (strpos($insideFolders , '(')){
					parseParrentFolder($insideFolders, $header ); 
				} else{
					$elems = explode(",", $insideFolders);

					foreach ($elems as $elem){

						$arr[] = $header.$elem;
					}
					
				}
			
				$boolparse = false;
			}
		}
	}
}
